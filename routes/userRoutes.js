
const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");
const auth = require("../auth");


// Route for checking if the user's email already exists in the database
//  pass the "body" property of our request 

router.post("/checkEmail", (req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})

// Route for user registration

router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(
		resultFromController));
})

// Route for user authentication

router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});


// Activity s38

/*
router.post("/details",(req,res)=>{
	userController.getProfile({id: req.body.id}).then(resultFromController=>res.send(resultFromController));
});
*/


// Provided Solution

/*
A)

router.post("/details",(req,res)=>{
	userController.getProfile(req.body).then(resultFromController=>res.send(resultFromController));
});


B)
*/

router.get("/details", auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController=>res.send(resultFromController));
});



// Route to enroll a user into a course


// router.post("/enroll", (req,res)=>{
// 	let data = {
// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController=>res.send(resultFromController));

// })




// s41 Activity

// Implementing user authentication for enroll route

router.post("/enroll", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let data = {
        userId: userData.id,
        courseId: req.body.courseId
    };


    userController.enroll(data).then(resultFromController => res.send(resultFromController));

})


module.exports = router;
