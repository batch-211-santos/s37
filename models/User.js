

const mongoose = require ("mongoose");

const userSchema = new mongoose.Schema({
        firstName: {
            type: String,
            required: [true, "Please enter your first name."]
        },
        lastName: {
            type: String,
            required: [true, "Please enter your last name."]
        },
        email: {
            type: String,
            required: [true, "Please enter your email address."]
        },
        password: {
            type: String,
            required: [true, "Please enter your passowrd."]
        },
        isAdmin: {
            type: Boolean,
            default: false
        },
        mobileNo: {
            type: String,
            required: [true, "Please enter your mobile number."]
        },
        enrollments: [
            {
                courseId: {
                   type: String,
                   required: [true, "CourseId is required."]
                }, 
                enrolledOn: {
                    type: Date,
                    default: new Date()
                },
                status: {
                	type: String,
                	default: "Enrolled"
                }
            }
        ]
});

module.exports = mongoose.model("User", userSchema);