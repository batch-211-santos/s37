


const mongoose = require ("mongoose");

const courseSchema = new mongoose.Schema({
        name: {
            type: String,
            required: [true, "Please enter the course name."]
        },
        description: {
            type: String,
            required: [true, "Please enter the course description."]
        },
        price: {
            type: Number,
            required: [true, "Please enter the course price."]
        },
        isActive: {
            type: Boolean,
            default: true
        },
        createdOn: {
            type: Date,
            default: new Date()
        },
        enrollees: [
            {
                userId: {
                   type: String,
                   required: [true, "UserId is required."]
                }, 
                enrolledOn: {
                    type: Date,
                    default: new Date()
                }
            }
        ]
});

module.exports = mongoose.model("Course", courseSchema);

