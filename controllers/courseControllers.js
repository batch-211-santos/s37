
const Course = require("../models/Course");


// Create a new course

/*

	1. Create a conditional statement that will check if the user is an admin.
	2. Create a new Course object using mongoose model and the information from the reqBody and the id from the header
	3. Save the new Course in the database

     *only isAdmin: true can create course
*/


/*
module.exports.addCourse = (reqBody) => {

	if (reqBody.isAdmin == true) {
		let newCourse = new Course({
			name : reqBody.course.name,
			description : reqBody.course.description,
			price : reqBody.course.price
		});
			return newCourse.save().then((course,error)=>{
				if(error){
					return false;
				}else{
					return true;
				};
			});
	} else {
		return false;
	};

};

*/

// Solution 1

/*
module.exports.addCourse = (reqBody) => {

		let newCourse = new Course({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

			if(reqBody.isAdmin){

				return newCourse.save().then((course,error)=>{
					if (error) {
						return false;
					} else {
						return true;
					};
			});
	
		} else {
			return false;
		};
};
*/

// Solution 2

module.exports.addCourse = (data) => {

	console.log(data);

	if (data.isAdmin) {
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

			console.log(newCourse);
			return newCourse.save().then((course,error)=>{
				if (error) {
					return false;
				} else {
					return true;
				};
			});

	} else {
		return false;
	};

};


// Retrieve all courses

/*
	1. Retrieve all the courses from the database
		Model.find({})

*/

module.exports.getAllCourses = () => {
	return Course.find({}).then(result=>{
		return result;
	});
};


// Retrieve all Active Courses

/*
	1. Retrieve all the from the database with the property of "isActive" to true
*/

module.exports.getAllActive = () => {
	return Course.find({isActive:true}).then(result=>{
		return result;
	});
};


// Retrieving a specific course
/*
	1. Retrieve the course that matches the course ID provided fromt the URL
*/

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	});
};


// Update a course

/*
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body

*/


module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse= {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error)=>{
		if (error){
			return false;
		} else {
			return true;
		}
	})
};


// s40 Activity


module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	};
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course,error)=>{
		if (error){
			return false;
		} else {
			return true;
		}
	})
};


// Solution 2

/*

module.exports.archiveCourse = (reqParams) => {
	
	return Course.findByIdAndUpdate(reqParams,{isActive.false}).then((course,error)=>{
		if (error){
			return false;
		} else {
			return true;
		}
	})
};


- Stretch goal

module.exports.archiveCourse = (reqParams) => {
	
	return Course.findByIdAndUpdate(reqParams,{isActive.true}).then((course,error)=>{
		if (error){
			return false;
		} else {
			return true;
		}
	})
};


*/

